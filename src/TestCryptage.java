import java.io.File;
import javax.crypto.Cipher;

public class TestCryptage {
	public static void main(String[] args) {
		 String key = "Cl� dee s�curit�" ;
		 String algorithm = "AES";
		 String transformation = "AES";
		 
		 File inputFile = new File("./src/fichier1.txt" );
		 File encryptedFile = new File( "./src/documentCrypte.txt" );
		 File decryptedFile = new File( "./src/documentDecrypte.txt" );
		 
		 //Op�ration de chifrement
		 CryptageTraitement.traitement(Cipher.ENCRYPT_MODE,key, algorithm, transformation, inputFile, encryptedFile);
		 //Op�ration de d�chifrement
		 CryptageTraitement.traitement(Cipher.DECRYPT_MODE,key, algorithm, transformation, encryptedFile, decryptedFile);
		}
}